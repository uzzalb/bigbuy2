import 'package:flutter/material.dart';

class Home_HotDeal_Section extends StatelessWidget {
   Home_HotDeal_Section({Key? key,
    required this.mainImage,
    required this.mainPrice,
    required this.cashbackAmount,
    required  this.productName,
    // required this.qty,
    this.subCategoryId,
    required   this.productCode,
    this.status,
    this.ipAddress,
    this.addBy,
    this.addTime,
    this.brand,
    this.brandName,
    this.cashbackPercent,
    this.isFeatured,
    this.isHotDeals,
    this.isNewArrival,
    this.productBranchid,
    this.productCategoryID,
    this.productCategoryName,
    required this.productDescription,
    this.productShippingReturns,
    this.productSlNo,
    this.productSubCategoryName,
    this.productSubSubCategoryName,
    required this.salePrice,
    this.slug,
    this.stock,
    this.subSubCategoryId,
    this.unitID,
    this.unitName,
    this.updateBy,
    this.updateTime,
  }) : super(key: key);
  String? productSlNo;
  String? productCode;
  String? productName;
  String? slug;
  String? productCategoryID;
  String? subCategoryId;
  String? subSubCategoryId;
  String? brand;
  String? salePrice;
  String? mainPrice;
  String? cashbackPercent;
  String? cashbackAmount;
  String? productDescription;
  String? productShippingReturns;
  String? stock;
  String? unitID;
  String? mainImage;
  String? isFeatured;
  String? isHotDeals;
  String? isNewArrival;
  String? status;
  String? addBy;
  String? addTime;
  String? updateBy;
  String? updateTime;
  String? ipAddress;
  String? productBranchid;
  String? qty;
  String? productCategoryName;
  String? productSubCategoryName;
  String? productSubSubCategoryName;
  String? brandName;
  String? unitName;
  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 8,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10),
      ),
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
        ),
        child:Column(
          children: [
            Expanded(
                flex:5,
                child: Container(
                  height: double.infinity,
                  width: double.infinity,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    image: DecorationImage(image: AssetImage("assets/icons/products/plastic_chair@2x.png"),fit: BoxFit.fill,),
                  ),
                 child: Image.network("https://bigbuy.com.bd/uploads/products/small_image/${mainImage}",fit: BoxFit.fill,),
                )),
            Expanded(
                flex:2,
                child: Container(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        children: [
                          Container(
                            child: Text("  Price : ",style: TextStyle(
                              fontSize: 12,
                              fontWeight: FontWeight.bold,
                            ),),
                          ),
                          Text("${salePrice}",style: TextStyle(
                            fontSize: 10,
                            fontWeight: FontWeight.w400,
                          ),),
                        ],
                      ),
                      Container(
                        margin: EdgeInsets.only(
                            left: 5,
                            right: 5
                        ),
                        width: double.infinity,
                        alignment: Alignment.center,
                        color: Color(0xff076BD6),
                        child: Text("Add to cart",style: TextStyle(

                            color: Colors.white
                        ),),
                      )
                    ],
                  ),
                )),
          ],
        ) ,
      ),
    );
  }
}
