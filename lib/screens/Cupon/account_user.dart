import 'package:bigbuy/Provider/SubCategory_Provider/all_sub_category_product.dart';
import 'package:flutter/material.dart';

import 'package:provider/provider.dart';

class FutureBuilderTest extends StatefulWidget {
  const FutureBuilderTest({Key? key}) : super(key: key);

  @override
  State<FutureBuilderTest> createState() => _FutureBuilderTestState();
}

class _FutureBuilderTestState extends State<FutureBuilderTest> {
  @override
  Widget build(BuildContext context) {
    final all_subcategory_productlist=Provider.of<All_SubCategory_Product_Provider>(context).brandList;
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text("Streambuilder"),
      ),
      body: Consumer<All_SubCategory_Product_Provider>(
        builder: (context, value, child) {
        return Container(
            height: double.infinity,
            width: double.infinity,
            child: ListView.builder(
              itemCount: all_subcategory_productlist.length,
              itemBuilder: (context, index) {
              return Text("${value.brandList[index].productName}");
            },),
          );
        },

      ),
    );
  }
}
