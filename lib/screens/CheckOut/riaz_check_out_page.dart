import 'package:bigbuy/CustomTextFormField/riaz_custom_text_form_field.dart';
import 'package:bigbuy/Provider/All_Product_Provider/all_product_provider.dart';
import 'package:bigbuy/screens/Login_SignUp/pages/signin_page.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:get_storage/get_storage.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:provider/provider.dart';

class CheckoutScreen extends StatefulWidget {
  late String checkoutFrom;
  String? productName,cashBackPersen,Product_id;
  int ? productQuantity;
  double? productUnitPrice, productSubtotalPrice, productTotalPrice,totalmainprice,cashbackammount;
  int ? Size_id,Collor_id;



  CheckoutScreen({
    super.key,
    required this.checkoutFrom,
    this.productName,
    this.productUnitPrice,
    this.productQuantity,
    this.productSubtotalPrice,
    this.productTotalPrice,
    this.Collor_id,
    this.Size_id,
    this.cashbackammount,
    this.totalmainprice,
    this.cashBackPersen,
    this.Product_id,
  });

  @override
  State<CheckoutScreen> createState() => _CheckoutScreenState();
}

class _CheckoutScreenState extends State<CheckoutScreen> {
  List<String> _locations = ['Uttara', 'Gulshan', 'Banani', 'Dhanmondi'];
  String? _selectedLocation;
  bool showWalletPay = false;
  String deliveryOptions = "Cash on delivery";

  final _formKey = GlobalKey<FormState>();

  TextEditingController _shipperNameController = TextEditingController();
  TextEditingController _shipperPhoneController = TextEditingController();
  TextEditingController _shipperAddressController = TextEditingController();
  TextEditingController _orderNoteController = TextEditingController();

  final _listViewController = ScrollController();
  final _scrollViewController = ScrollController();
  late final Box box;
  double shippingFee = 60;
  bool isWalletAdjusted = false;
  int _selectedRadioButton = 0;


  Fatch_Orderrr(context)async{
    String link ="https://bigbuy.com.bd/api/place_order";

    try{
      Response response=await Dio().post(link,
        data:{
          "customer_id" : "${GetStorage().read("Customer_SlNo")}",
          "customer_name" : "${_shipperNameController.text}",
          "wallet_upto_adjustment_percent" :100,
          "previous_wallet_balance" : 100,
          "bound" :"Bangladesh",
          "customer_phone":"${_shipperPhoneController.text}",
          "customer_email" : "uzzal.171.cse@gmail.com",
          "shipping_address" : "${GetStorage().read("Customer_Address")}",
          "order_note" : "${_orderNoteController.text}",
          "sub_total" :"${widget.productSubtotalPrice}",
          "cashback" :"${widget.totalmainprice}",
          "vat" :10,
          "delivery_charge" :60,
          "total" :"${(widget.productSubtotalPrice)!+60}",
          "wallet_adjustment_amount" :100,
          "payable_amount" :100,
          "payment_type" : "Cash On Delivery",
          "is_wallet_adjustment" :1 ,
          "wallet_adjustment_percent":30,
          "vat_percent" :10,
          "area_id" :1,
          "delivery_option_id":1,

          "cart" : [
            {
              "product_id" : widget.Product_id,
              "color_id" :widget.Collor_id,
              "size_id" :widget.Size_id,
              "purchase_rate" :20,
              "main_price" :"${widget.productUnitPrice}",
              "quantity" :"${widget.productQuantity}",
              "total_main_price" :"${(widget.productSubtotalPrice)}",
              "cashback_percent":"${widget.cashBackPersen}",
              "cashback" :"${widget.totalmainprice}",
              "total_cashback" :"${widget.totalmainprice}"
            }
          ]
        },
        options: Options(
          headers: {
            "X-API-KEY":"${GetStorage().read("key")}"// Set the content-length.
          },
        ),
      );

      print(response.data);



    }catch(e){
      print(e);
    }

  }

  @override
  void initState() {
    _listViewController.addListener(_onListScroll);
    box = Hive.box('productBox');
    print("key ${GetStorage().read("key")}");
    print("Customer_SlNo ${GetStorage().read("Customer_SlNo")}");
    print("Customer_Name ${GetStorage().read("Customer_Name")}");
    print("Customer_Mobile ${GetStorage().read("Customer_Mobile")}");
    print("Customer_Address ${GetStorage().read("Customer_Address")}");
    print("country_id ${GetStorage().read("country_id")}");
    print("image_name ${GetStorage().read("image_name")}");
    print("Customer_Email ${GetStorage().read("Customer_Email")}");
    print("wallet_balance ${GetStorage().read("wallet_balance")}");
    print("referral_id ${GetStorage().read("referral_id")}");

    Future.delayed(Duration(milliseconds: 100),()async {
      _shipperNameController.text=GetStorage().read("Customer_Name");
      _shipperPhoneController.text=GetStorage().read("Customer_Mobile");
      _shipperAddressController.text=GetStorage().read("Customer_Address");
    },);


    print("chekout from==${widget.checkoutFrom}");
    print("productName==${widget.productName}");
    print("productQuantity==${widget.productQuantity}");
    print("productUnitPrice==${widget.productUnitPrice}");
    print("productSubtotalPrice==${widget.productSubtotalPrice}");
    print("productTotalPrice==${widget.productTotalPrice}");
    print("Size_Iddd==${widget.Size_id}");
    print("Color_Iddd==${widget.Collor_id}");
    print("cashbackammount==${widget.cashbackammount}");
    print("cashbackammount==${widget.totalmainprice}");
    print("cashbackammount==${widget.cashBackPersen}");
    print("cashbackammount==${widget.Product_id}");
    print("=============================");
    Provider.of<All_Product_Provider>(context,listen: false).Api_area_wise_delivery_charge(context);
    super.initState();
  }

  @override
  void dispose() {
    _listViewController.removeListener(_onListScroll);
    super.dispose();
  }

  void _onListScroll() {
    if (_listViewController.position.pixels == _listViewController.position.maxScrollExtent) {
      _scrollViewController.animateTo(
        _scrollViewController.position.maxScrollExtent,
        duration: const Duration(milliseconds: 50),
        curve: Curves.easeIn,
      );
    }

    if (_listViewController.position.pixels == _listViewController.position.minScrollExtent) {
      _scrollViewController.animateTo(
        _scrollViewController.position.minScrollExtent,
        duration: const Duration(milliseconds: 50),
        curve: Curves.easeIn,
      );
    }
  }

  //==============================================

  @override
  Widget build(BuildContext context) {

    print("key ${GetStorage().read("key")}");
    print("Customer_SlNo ${GetStorage().read("Customer_SlNo")}");
    print("Customer_Name ${GetStorage().read("Customer_Name")}");
    print("Customer_Mobile ${GetStorage().read("Customer_Mobile")}");
    print("Customer_Address ${GetStorage().read("Customer_Address")}");
    print("country_id ${GetStorage().read("country_id")}");
    print("image_name ${GetStorage().read("image_name")}");
    print("Customer_Email ${GetStorage().read("Customer_Email")}");
    print("wallet_balance ${GetStorage().read("wallet_balance")}");
    print("referral_id ${GetStorage().read("referral_id")}");


    double h2TextSize = 15.0;
    var screenHeight = MediaQuery.of(context).size.height;
    var screenWidth = MediaQuery.of(context).size.width;
  final Api_area_wise_delivery_charge_list=  Provider.of<All_Product_Provider>(context).Api_area_wise_delivery_charse_list;
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        leading: InkWell(
            onTap: () {
              Navigator.pop(context);
            },
            child: Icon(Icons.arrow_back)),
        toolbarHeight: 45,
        backgroundColor: Color.fromARGB(255, 87, 117, 133),
        title: Text('Bigbuy Checkout Page'),
      ),
      body: SingleChildScrollView(
          controller: _scrollViewController,
          child: widget.checkoutFrom == "cart_page"
              ? Container(
                  padding: EdgeInsets.all(10),
                  // color: Colors.amber,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      ListView.builder(
                        controller: _listViewController,
                        physics: ClampingScrollPhysics(),
                        shrinkWrap: true,
                        itemCount: box.length,
                        itemBuilder: (context, index) {
                          var currentBox = box;
                          var productData = currentBox.getAt(index)!;
                          print(
                            "product length: ${box.length}",
                          );
                          return Column(
                            children: [
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Container(
                                    width: screenWidth / 3 + 20,
                                    child: Text(
                                      "${productData.productName}",
                                      style: TextStyle(fontSize: h2TextSize, fontWeight: FontWeight.bold, color: Colors.black38),
                                    ),
                                  ),
                                  Container(
                                    width: screenWidth / 12,
                                    child: Text(
                                      "${productData.productQuantity}",
                                      style: TextStyle(fontSize: h2TextSize, fontWeight: FontWeight.bold, color: Colors.black38),
                                    ),
                                  ),
                                  Container(
                                    width: screenWidth / 5,
                                    child: Text(
                                      "৳${productData.productPrice}",
                                      textAlign: TextAlign.start,
                                      style: TextStyle(fontSize: h2TextSize, fontWeight: FontWeight.bold, color: Colors.black38),
                                    ),
                                  ),
                                  Container(
                                      width: screenWidth / 4,
                                      child: Row(
                                        mainAxisAlignment: MainAxisAlignment.end,
                                        children: [
                                          Text(
                                            "৳",
                                            style: TextStyle(fontSize: h2TextSize, fontWeight: FontWeight.bold, color: Colors.black38),
                                          ),
                                          Text(
                                            "${productData.productPrice * productData.productQuantity}",
                                            textAlign: TextAlign.start,
                                            style: TextStyle(fontSize: h2TextSize, fontWeight: FontWeight.bold, color: Colors.black38),
                                          ),
                                        ],
                                      ))
                                ],
                              ),
                              Divider()
                            ],
                          );
                        },
                      ),
                      Container(
                        margin: EdgeInsets.only(bottom: 15),
                        decoration: BoxDecoration(
                          color: const Color.fromARGB(255, 231, 231, 231),
                          borderRadius: BorderRadius.circular(10),
                        ),
                        child: ListView.builder(
                          itemCount: 1,
                          shrinkWrap: true,
                          physics: ClampingScrollPhysics(),
                          itemBuilder: (BuildContext context, int index) {
                            num totalPrice = 0;
                            for (int i = 0; i < box.length; i++) {
                              var productData = box.getAt(i)!;
                              totalPrice += productData.productPrice * productData.productQuantity;
                            }
                            return Container(
                              padding: EdgeInsets.all(20),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: [
                                      Text(
                                        "Subtotal: ",
                                        style: TextStyle(
                                          fontSize: h2TextSize,
                                        ),
                                      ),
                                      Text(
                                        "৳$totalPrice",
                                        style: TextStyle(fontSize: h2TextSize, fontWeight: FontWeight.bold, color: Colors.black38),
                                      )
                                    ],
                                  ),
                                  Divider(),
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: [
                                      Text(
                                        "Cashback: ",
                                        style: TextStyle(
                                          fontSize: h2TextSize,
                                        ),
                                      ),
                                      Text(
                                        "${widget.totalmainprice}",
                                        style: TextStyle(fontSize: h2TextSize, color: Colors.black),
                                      )
                                    ],
                                  ),
                                  Divider(),
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: [
                                      Text(
                                        "Vat 15% (+): ",
                                        style: TextStyle(
                                          fontSize: h2TextSize,
                                        ),
                                      ),
                                      Text(
                                        "৳180.00",
                                        style: TextStyle(fontSize: h2TextSize, fontWeight: FontWeight.bold, color: Colors.black38),
                                      )
                                    ],
                                  ),
                                  Divider(),
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: [
                                      Text(
                                        "Delivery Charge (+):",
                                        style: TextStyle(
                                          fontSize: h2TextSize,
                                        ),
                                      ),
                                      Text(
                                        "৳$shippingFee",
                                        style: TextStyle(fontSize: h2TextSize, fontWeight: FontWeight.bold, color: Colors.black38),
                                      ),
                                    ],
                                  ),
                                  Divider(),
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: [
                                      Text(
                                        "Area:",
                                        style: TextStyle(
                                          fontSize: h2TextSize,
                                        ),
                                      ),
                                      DropdownButton(
                                        hint: Text(
                                          'Please choose a location',
                                          style: TextStyle(
                                            fontSize: h2TextSize,
                                          ),
                                        ), // Not necessary for Option 1
                                        value: _selectedLocation,
                                        onChanged: (newValue) {
                                          setState(() {
                                             _selectedLocation = newValue!;
                                          });
                                        },

                                        items: _locations.map((location) {
                                          return DropdownMenuItem(
                                            child: Text(
                                            location,
                                              style: TextStyle(
                                                fontSize: h2TextSize,
                                              ),
                                            ),
                                            value: location,
                                          );
                                        }).toList(),
                                      ),
                                    ],
                                  ),
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: [
                                      Text(
                                        "Total :",
                                        style: TextStyle(
                                          fontSize: h2TextSize,
                                        ),
                                      ),
                                      Text(
                                        "৳${(totalPrice + shippingFee).toString()}",
                                        style: TextStyle(fontSize: h2TextSize, color: Colors.black),
                                      ),
                                    ],
                                  ),
                                  showWalletPay == true
                                      ? Column(
                                          children: [
                                            Divider(),
                                            Row(
                                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                              children: [
                                                Text(
                                                  "Wallet Pay :",
                                                  style: TextStyle(
                                                    fontSize: h2TextSize,
                                                  ),
                                                ),
                                                Text(
                                                  "33.0",
                                                  style: TextStyle(fontSize: h2TextSize, color: Colors.black),
                                                ),
                                              ],
                                            )
                                          ],
                                        )
                                      : Container(),
                                  Divider(),
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: [
                                      Text(
                                        "Payable :",
                                        style: TextStyle(
                                          fontSize: h2TextSize,
                                        ),
                                      ),
                                      Text(
                                        "৳${(totalPrice + shippingFee).toString()}",
                                        style: TextStyle(fontSize: h2TextSize, fontWeight: FontWeight.bold, color: Colors.black),
                                      ),
                                    ],
                                  ),
                                  Divider(),
                                  Row(
                                    children: [
                                      Checkbox(
                                        checkColor: Colors.white,
                                        activeColor: Colors.black,
                                        value: isWalletAdjusted,
                                        onChanged: (bool? value) {
                                          setState(() {
                                            isWalletAdjusted = value!;
                                            if (isWalletAdjusted) {
                                              showWalletPay = true;
                                            } else {
                                              showWalletPay = false;
                                            }
                                          });
                                        },
                                      ),
                                      SizedBox(
                                        width: 5.0,
                                      ),
                                      Text(
                                        'Wallet Adjustment (33%)',
                                        style: TextStyle(
                                          fontSize: screenWidth / 24,
                                          color: Colors.lightGreen,
                                        ),
                                      ),
                                    ],
                                  ),
                                  Divider(),
                                  Row(
                                    children: [
                                      Expanded(
                                        flex: 1,
                                        child: Row(
                                          children: [
                                            Radio(
                                              value: 0,
                                              groupValue: _selectedRadioButton,
                                              onChanged: (value) {
                                                setState(() {
                                                  _selectedRadioButton = value!;
                                                });
                                              },
                                            ),
                                            Text(
                                              'Cash on delivery',
                                              style: TextStyle(
                                                fontWeight: FontWeight.bold,
                                                color: Colors.black54,
                                                fontSize: screenWidth / 30,
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                      Expanded(
                                        flex: 1,
                                        child: Row(
                                          children: [
                                            Radio(
                                              value: 1,
                                              groupValue: _selectedRadioButton,
                                              onChanged: (value) {
                                                setState(() {
                                                  _selectedRadioButton = value!;
                                                });
                                              },
                                            ),
                                            Text(
                                              'Online Payment',
                                              style: TextStyle(
                                                fontWeight: FontWeight.bold,
                                                color: Colors.black54,
                                                fontSize: screenWidth / 30,
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ],
                                  )
                                ],
                              ),
                            );
                          },
                        ),
                      ),
                      //Shipping Information Section
                      Text('Shipping Information', style: TextStyle(fontSize: h2TextSize, fontWeight: FontWeight.bold, color: Colors.black54)),
                      Container(
                        margin: EdgeInsets.only(top: 15),
                        child: Form(
                          key: _formKey,
                          child: Column(
                            children: [
                              Container(
                                padding: const EdgeInsets.all(8.0),
                                decoration: BoxDecoration(color: Color.fromARGB(255, 231, 231, 231), borderRadius: BorderRadius.circular(10)),
                                child: Column(
                                  children: [
                                    Row(
                                      children: [
                                        Expanded(flex: 1, child: Text("Name *", style: TextStyle(fontSize: h2TextSize))),
                                        SizedBox(
                                          width: 5,
                                        ),
                                        Expanded(
                                            flex: 3,
                                            child: MyCustomTextFormField().getCustomEditTextArea(
                                                hintValue: 'Name',
                                                validation: true,
                                                controller: _shipperNameController,
                                                keyboardType: TextInputType.text,
                                                validationErrorMsg: 'error_msg')),
                                      ],
                                    ),
                                    Row(
                                      children: [
                                        Expanded(flex: 1, child: Text("Phone number *", style: TextStyle(fontSize: h2TextSize))),
                                        SizedBox(
                                          width: 5,
                                        ),
                                        Expanded(
                                            flex: 3,
                                            child: MyCustomTextFormField().getCustomEditTextArea(
                                                hintValue: 'Phone number',
                                                validation: true,
                                                controller: _shipperPhoneController,
                                                keyboardType: TextInputType.phone,
                                                validationErrorMsg: 'error_msg')),
                                      ],
                                    ),
                                    Row(
                                      children: [
                                        Expanded(flex: 1, child: Text("Shipping Address *", style: TextStyle(fontSize: h2TextSize))),
                                        SizedBox(
                                          width: 5,
                                        ),
                                        Expanded(
                                            flex: 3,
                                            child: MyCustomTextFormField().getCustomEditTextArea(
                                                hintValue: 'Shipping Address',
                                                validation: true,
                                                controller: _shipperAddressController,
                                                keyboardType: TextInputType.text,
                                                validationErrorMsg: 'error_msg')),
                                      ],
                                    ),
                                    SizedBox(
                                      height: 5,
                                    ),
                                    Row(
                                      children: [
                                        Expanded(flex: 1, child: Text("Order notes (optional)", style: TextStyle(fontSize: h2TextSize))),
                                        SizedBox(
                                          width: 5,
                                        ),
                                        Expanded(
                                            flex: 3,
                                            child: MyCustomTextFormField().getCustomEditTextArea(
                                              hintValue: 'Notes about your order, e.g. special notes for delivery',
                                              validation: false,
                                              maxLineValue: 2,
                                              controller: _orderNoteController,
                                              keyboardType: TextInputType.text,
                                            )),
                                      ],
                                    ),
                                    SizedBox(
                                      height: 5,
                                    ),
                                    Align(
                                        alignment: Alignment.centerRight,
                                        child: ElevatedButton(
                                            onPressed: () {
                                              if (_formKey.currentState!.validate()) {}
                                            },
                                            child: Text("Update")))
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                )
              : Container(
                  padding: EdgeInsets.all(10),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Container(
                            width: screenWidth / 3 + 20,
                            child: Text(
                              "${widget.productName}",
                              style: TextStyle(fontSize: h2TextSize, fontWeight: FontWeight.bold, color: Colors.black38),
                            ),
                          ),
                          Container(
                            width: screenWidth / 12,
                            child: Text(
                              "${widget.productQuantity}",
                              style: TextStyle(fontSize: h2TextSize, fontWeight: FontWeight.bold, color: Colors.black38),
                            ),
                          ),
                          Container(
                            width: screenWidth / 5,
                            child: Text(
                              "৳${widget.productUnitPrice}",
                              textAlign: TextAlign.start,
                              style: TextStyle(fontSize: h2TextSize, fontWeight: FontWeight.bold, color: Colors.black38),
                            ),
                          ),
                          Container(
                              width: screenWidth / 4,
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.end,
                                children: [
                                  Text(
                                    "৳",
                                    style: TextStyle(fontSize: h2TextSize, fontWeight: FontWeight.bold, color: Colors.black38),
                                  ),
                                  Text(
                                    "${widget.productSubtotalPrice}",
                                    textAlign: TextAlign.start,
                                    style: TextStyle(fontSize: h2TextSize, fontWeight: FontWeight.bold, color: Colors.black38),
                                  ),
                                ],
                              ))
                        ],
                      ),
                      Divider(),

                      //Bill Section
                      // Container(
                      //   height: 90,
                      //   decoration: BoxDecoration(color: const Color.fromARGB(255, 231, 231, 231), borderRadius: BorderRadius.circular(10)),
                      //   child: ListView.builder(
                      //     itemCount: 1,
                      //     itemBuilder: (BuildContext context, int index) {
                      //       return Center(
                      //         child: Container(
                      //           padding: EdgeInsets.only(top: 10, left: 12, right: 12, bottom: 20),
                      //           child: Column(
                      //             crossAxisAlignment: CrossAxisAlignment.start,
                      //             children: [
                      //               Row(
                      //                 mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      //                 children: [
                      //                   Text(
                      //                     "Subtotal: ",
                      //                     style: TextStyle(fontSize: h2TextSize, fontWeight: FontWeight.bold, color: Colors.black38),
                      //                   ),
                      //                   Text(
                      //                     "৳${widget.productSubtotalPrice}",
                      //                     style: TextStyle(fontSize: h2TextSize, fontWeight: FontWeight.bold, color: Colors.black38),
                      //                   )
                      //                 ],
                      //               ),
                      //               Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
                      //                 Text(
                      //                   "Shipping cost:",
                      //                   style: TextStyle(fontSize: h2TextSize, fontWeight: FontWeight.bold, color: Colors.black38),
                      //                 ),
                      //                 Text(
                      //                   "৳ $shippingFee",
                      //                   style: TextStyle(fontSize: h2TextSize, fontWeight: FontWeight.bold, color: Colors.black38),
                      //                 ),
                      //               ]),
                      //               Row(
                      //                 mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      //                 children: [
                      //                   Text(
                      //                     "Total Price:",
                      //                     style: TextStyle(
                      //                       fontSize: h2TextSize,
                      //                     ),
                      //                   ),
                      //                   Text(
                      //                     "৳${(widget.productTotalPrice! + shippingFee)}",
                      //                     style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold, color: Colors.black),
                      //                   ),
                      //                 ],
                      //               ),
                      //             ],
                      //           ),
                      //         ),
                      //       );
                      //     },
                      //   ),
                      // ),

                      //Bill Section
                      Container(
                        padding: EdgeInsets.all(20),
                        margin: EdgeInsets.only(bottom: 15),
                        decoration: BoxDecoration(
                          color: const Color.fromARGB(255, 231, 231, 231),
                          borderRadius: BorderRadius.circular(10),
                        ),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text(
                                  "Subtotal: ",
                                  style: TextStyle(fontSize: h2TextSize, color: Colors.black),
                                ),
                                Text(
                                  "৳${widget.productSubtotalPrice}",
                                  style: TextStyle(fontSize: h2TextSize, color: Colors.black),
                                )
                              ],
                            ),
                            Divider(),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text(
                                  "Cashback: ",
                                  style: TextStyle(
                                    fontSize: h2TextSize,
                                  ),
                                ),
                                Text(
                                  "৳325.00",
                                  style: TextStyle(fontSize: h2TextSize, color: Colors.black),
                                )
                              ],
                            ),
                            Divider(),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text(
                                  "Vat 15% (+): ",
                                  style: TextStyle(
                                    fontSize: h2TextSize,
                                  ),
                                ),
                                Text(
                                  "৳180.00",
                                  style: TextStyle(fontSize: h2TextSize, color: Colors.black),
                                )
                              ],
                            ),
                            Divider(),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text(
                                  "Delivery Charge (+):",
                                  style: TextStyle(
                                    fontSize: h2TextSize,
                                  ),
                                ),
                                Text(
                                  "৳$shippingFee",
                                  style: TextStyle(fontSize: h2TextSize, color: Colors.black),
                                ),
                              ],
                            ),
                            Divider(),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text(
                                  "Area:",
                                  style: TextStyle(
                                    fontSize: h2TextSize,
                                  ),
                                ),
                                DropdownButton(
                                  hint: Text(
                                    'Please choose a location',
                                    style: TextStyle(
                                      fontSize: h2TextSize,
                                    ),
                                  ), // Not necessary for Option 1
                                  value: _selectedLocation,
                                  onChanged: (newValue) {
                                    setState(() {
                                      _selectedLocation = newValue!;
                                    });
                                  },

                                  items: _locations.map((location) {
                                    return DropdownMenuItem(
                                      child: Text(
                                        location,
                                        style: TextStyle(
                                          fontSize: h2TextSize,
                                        ),
                                      ),
                                      value: location,
                                    );
                                  }).toList(),
                                ),

                              ],
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text(
                                  "Total :",
                                  style: TextStyle(
                                    fontSize: h2TextSize,
                                  ),
                                ),
                                Text(
                                  "৳${(widget.productTotalPrice! + shippingFee)}",
                                  style: TextStyle(fontSize: h2TextSize, color: Colors.black),
                                ),
                              ],
                            ),
                            showWalletPay == true
                                ? Column(
                                    children: [
                                      Divider(),
                                      Row(
                                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                        children: [
                                          Text(
                                            "Wallet Pay :",
                                            style: TextStyle(
                                              fontSize: h2TextSize,
                                            ),
                                          ),
                                          Text(
                                            "33.0",
                                            style: TextStyle(fontSize: h2TextSize, color: Colors.black),
                                          ),
                                        ],
                                      )
                                    ],
                                  )
                                : Container(),
                            Divider(),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text(
                                  "Payable :",
                                  style: TextStyle(
                                    fontSize: h2TextSize,
                                  ),
                                ),
                                Text(
                                  "৳${(widget.productTotalPrice! + shippingFee).toString()}",
                                  style: TextStyle(fontSize: h2TextSize, fontWeight: FontWeight.bold, color: Colors.black),
                                ),
                              ],
                            ),
                            Divider(),
                            Row(
                              children: [
                                Checkbox(
                                  checkColor: Colors.white,
                                  activeColor: Colors.black,
                                  value: isWalletAdjusted,
                                  onChanged: (bool? value) {
                                    setState(() {
                                      isWalletAdjusted = value!;
                                      if (isWalletAdjusted) {
                                        showWalletPay = true;
                                      } else {
                                        showWalletPay = false;
                                      }
                                    });
                                  },
                                ),
                                SizedBox(
                                  width: 5.0,
                                ),
                                Text(
                                  'Wallet Adjustment (33%)',
                                  style: TextStyle(
                                    fontSize: screenWidth / 24,
                                    color: Colors.lightGreen,
                                  ),
                                ),
                              ],
                            ),
                            Divider(),
                            Row(
                              children: [
                                Expanded(
                                  flex: 1,
                                  child: Row(
                                    children: [
                                      Radio(
                                        value: 0,
                                        groupValue: _selectedRadioButton,
                                        onChanged: (value) {
                                          setState(() {
                                            _selectedRadioButton = value!;
                                          });
                                        },
                                      ),
                                      Text(
                                        'Cash on delivery',
                                        style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          color: Colors.black54,
                                          fontSize: screenWidth / 30,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                Expanded(
                                  flex: 1,
                                  child: Row(
                                    children: [
                                      Radio(
                                        value: 1,
                                        groupValue: _selectedRadioButton,
                                        onChanged: (value) {
                                          setState(() {
                                            _selectedRadioButton = value!;
                                          });
                                        },
                                      ),
                                      Text(
                                        'Online Payment',
                                        style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          color: Colors.black54,
                                          fontSize: screenWidth / 30,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            )
                          ],
                        ),
                      ),
                      //Shipping Information Section
                      Text('Shipping Information', style: TextStyle(fontSize: h2TextSize, fontWeight: FontWeight.bold, color: Colors.black54)),
                      Container(
                        margin: EdgeInsets.only(top: 15),
                        child: Form(
                          key: _formKey,
                          child: Column(
                            children: [
                              Container(
                                padding: const EdgeInsets.all(8.0),
                                decoration: BoxDecoration(color: Color.fromARGB(255, 231, 231, 231), borderRadius: BorderRadius.circular(10)),
                                child: Column(
                                  children: [
                                    Row(
                                      children: [
                                        Expanded(flex: 1, child: Text("Name *", style: TextStyle(fontSize: h2TextSize))),
                                        SizedBox(
                                          width: 5,
                                        ),
                                        Expanded(
                                            flex: 3,
                                            child: MyCustomTextFormField().getCustomEditTextArea(
                                                hintValue: 'Name',
                                                validation: true,
                                                controller: _shipperNameController,
                                                keyboardType: TextInputType.text,
                                                validationErrorMsg: 'error_msg')),
                                      ],
                                    ),
                                    Row(
                                      children: [
                                        Expanded(flex: 1, child: Text("Phone number *", style: TextStyle(fontSize: h2TextSize))),
                                        SizedBox(
                                          width: 5,
                                        ),
                                        Expanded(
                                            flex: 3,
                                            child: MyCustomTextFormField().getCustomEditTextArea(
                                                hintValue: 'Phone number',
                                                validation: true,
                                                controller: _shipperPhoneController,
                                                keyboardType: TextInputType.phone,
                                                validationErrorMsg: 'error_msg')),
                                      ],
                                    ),
                                    Row(
                                      children: [
                                        Expanded(flex: 1, child: Text("Shipping Address *", style: TextStyle(fontSize: h2TextSize))),
                                        SizedBox(
                                          width: 5,
                                        ),
                                        Expanded(
                                            flex: 3,
                                            child: MyCustomTextFormField().getCustomEditTextArea(
                                                hintValue: 'Shipping Address',
                                                validation: true,
                                                controller: _shipperAddressController,
                                                keyboardType: TextInputType.text,
                                                validationErrorMsg: 'error_msg')),
                                      ],
                                    ),
                                    SizedBox(
                                      height: 5,
                                    ),
                                    Row(
                                      children: [
                                        Expanded(flex: 1, child: Text("Order notes (optional)", style: TextStyle(fontSize: h2TextSize))),
                                        SizedBox(
                                          width: 5,
                                        ),
                                        Expanded(
                                            flex: 3,
                                            child: MyCustomTextFormField().getCustomEditTextArea(
                                              hintValue: 'Notes about your order, e.g. special notes for delivery',
                                              validation: false,
                                              maxLineValue: 2,
                                              controller: _orderNoteController,
                                              keyboardType: TextInputType.text,
                                            )),
                                      ],
                                    ),
                                    SizedBox(
                                      height: 5,
                                    ),
                                    Align(
                                        alignment: Alignment.centerRight,
                                        child: ElevatedButton(
                                            onPressed: () {
                                              if (_formKey.currentState!.validate()) {}
                                            },
                                            child: Text("Update")))
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                )),
      bottomNavigationBar: Padding(
        padding: const EdgeInsets.only(left: 20, right: 20, bottom: 10.0),
        child: ElevatedButton(
          style: ElevatedButton.styleFrom(backgroundColor: Color.fromARGB(255, 60, 84, 97), minimumSize: const Size.fromHeight(45)),
          onPressed: () {

            if(GetStorage().read("key")==null){
              Navigator.push(context, MaterialPageRoute(builder: (context) => SignInPage(isRemembered: true),));
            }else{
              Fatch_Orderrr(context);
            }




          },
          child: const Text("Proceed to Pay"),
        ),
      ),
    );
  }



}
