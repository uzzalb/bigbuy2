import 'package:bigbuy/Custom/Bootom_NavigationBar/bootom_navigation_bar.dart';
import 'package:bigbuy/Custom/Custom_Drtawer/custom_drawer.dart';
import 'package:bigbuy/Custom/Custom_Drtawer/end_drawer.dart';
import 'package:bigbuy/custom_page_view.dart';
import 'package:bigbuy/screens/Login_SignUp/pages/register_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:google_fonts/google_fonts.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({super.key});

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  int _selectedRadioButton = 0;
  bool isRemembered = false;
  var emailPhoneText = "Email";
  final _key=GlobalKey<ScaffoldState>();
  final PageController _controller = PageController();
  final _email_or_pnController=TextEditingController();
  @override
  Widget build(BuildContext context) {
    var screenHeight = MediaQuery.of(context).size.height;
    var screenWidth = MediaQuery.of(context).size.width;
    var paddingg = MediaQuery.of(context).size.height;
    return Scaffold(
      floatingActionButton: FloatingActionButton(
          onPressed: () {
            Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => CustomPageView(selectedIndex: 0),));
          },child: Icon(Icons.home,size: 27,)),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,

      bottomNavigationBar: CustomNavigationBarPage(
          Home_color: Colors.white,
          Produc_tColor: Colors.white,
          Category_color: Colors.white,
          setting_Color: Colors.black),
      key: _key,
      endDrawer: End_Add_to_cart_Drawer(),
      drawer: Custom_Drawer_Page(),
      appBar: AppBar(
        backgroundColor: Colors.white,
        leading: IconButton(onPressed:(){
          Navigator.pop(context);
        }, icon: Icon(Icons.arrow_back,size: 25,color: Colors.black87,),
        ),
        title: Text("BigBuy",style: GoogleFonts.poppins(
          fontSize: 18,
          fontStyle: FontStyle.italic,
          letterSpacing: 1,
          fontWeight: FontWeight.bold,
          color: Colors.black,
        ),),

        actions: [
          IconButton(
            iconSize: 28,
            icon: Icon(Icons.search,size: 25,color: Colors.black87,),
            onPressed: () {},
          ),
          IconButton(
            iconSize: 28,
            icon:Icon(Icons.shopping_cart,size: 25,color: Colors.black87,),
            onPressed: () {
              _key.currentState!.openEndDrawer();
            },
          ),
        ],
      ),
      body: Container(
        height: double.infinity,
        width: double.infinity,
        padding: EdgeInsets.only(top: paddingg/10),
        color: Color.fromARGB(239, 12, 1, 112),
        child: Container(
          height: double.infinity,
          width: double.infinity,
          padding: EdgeInsets.only(left: 30.0,top: paddingg/10),
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.only(topLeft: Radius.circular(100.0)),
          ),
          child: SingleChildScrollView(
            child:Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Radio(
                          value: 0,
                          groupValue: _selectedRadioButton,
                          onChanged: (value) {
                            setState(() {
                              _selectedRadioButton = value!;
                            });
                            // _controller.jumpToPage(value!);
                          },
                        ),
                        Text(
                          'Bangladesh',
                          style: GoogleFonts.poppins(
                            textStyle: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: screenWidth / 24,
                              color: Colors.black54,
                            ),
                          ),
                        ),
                        Radio(
                          value: 1,
                          groupValue: _selectedRadioButton,
                          onChanged: (value) {
                            setState(() {
                              _selectedRadioButton = value!;
                            });
                            // _controller.jumpToPage(value!);

                          },
                        ),
                        Text(
                          'International',
                          style: GoogleFonts.poppins(
                            textStyle: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: screenWidth / 24,
                              color: Colors.black54,
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 100,
                  ),
                  Text(
                    'Welcome',
                    style: TextStyle(
                        color: Colors.black,
                        fontSize: 30.0,
                        fontWeight: FontWeight.w600),
                  ),
                  SizedBox(height: 10.0),
                  Text(
                      'Welcome to Bada Business Commuinity. Enter your mobile number to register to with us'),
                  SizedBox(height: 10.0),
                  Row(

                    children: [
                      Container(
                          child: _selectedRadioButton==0?Row(
                            children: [
                              Image.asset(
                                'images/flag.jpg',
                                height: 20.0,
                                width: 25.0,
                              ),
                              Text('+880'),
                              Icon(
                                Icons.arrow_drop_down,
                                color: Colors.black,
                              ),
                            ],
                          ):Text("" ),
                      ),


                      Expanded(
                        flex: 8,
                        child: Padding(
                          padding: const EdgeInsets.only(right: 30.0),
                          child: TextField(
                            controller: _email_or_pnController,
                            keyboardType: TextInputType.emailAddress,
                            decoration: InputDecoration(
                              hintText: _selectedRadioButton==0? "Enter phone":"Enter email",
                              hintStyle: TextStyle(
                                  color: Colors.black38,
                                  fontSize: 16.0,
                                  fontWeight: FontWeight.w500),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(height: 60.0),
                  Center(
                    child: InkWell(
                      onTap: () {
                        Navigator.push(context, MaterialPageRoute(builder: (context) => RegisterPage(
                            selectedRegister: _selectedRadioButton,
                           emailorpn: _email_or_pnController.text,
                        ),));
                      },
                      child: Container(
                        height: 45.0,
                        width: MediaQuery.of(context).size.width / 2,
                        decoration: BoxDecoration(
                          gradient: LinearGradient(
                            begin: Alignment.topRight,
                            end: Alignment.bottomLeft,
                            colors: [
                              Color.fromARGB(255, 5, 126, 224),
                              Color.fromARGB(255, 5, 126, 224),
                              Color.fromARGB(255, 61, 157, 236),
                              Color.fromARGB(255, 64, 157, 233),
                            ],
                          ),
                          boxShadow: [
                            BoxShadow(
                                offset: Offset(0, 3),
                                blurRadius: 15.0,
                                spreadRadius: 3.0,
                                color: Colors.grey),
                          ],
                          color: Colors.blue,
                          borderRadius: BorderRadius.circular(10.0),
                        ),
                        child: Center(
                          child: Text(
                            'Confirm & Continue',
                            style: TextStyle(
                                color: Color.fromARGB(255, 241, 235, 235),
                                fontSize: 15.0,
                                fontWeight: FontWeight.w600),
                          ),
                        ),
                      ),
                    ),
                  )
                ]),
          ),
        ),
      ),
    );
  }
}
