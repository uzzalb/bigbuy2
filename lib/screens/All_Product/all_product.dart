import 'package:bigbuy/Custom/Bootom_NavigationBar/bootom_navigation_bar.dart';
import 'package:bigbuy/Custom/Custom_Appbar/custom_appbar.dart';
import 'package:bigbuy/Custom/Custom_Card/custom_card.dart';
import 'package:bigbuy/Custom/Custom_Drtawer/custom_drawer.dart';
import 'package:bigbuy/Custom/Custom_Drtawer/end_drawer.dart';
import 'package:bigbuy/FooterSection/about_section.dart';
import 'package:bigbuy/Provider/All_Product_Provider/all_product_provider.dart';
import 'package:bigbuy/screens/One_Product_Details/one_product_details.dart';
import 'package:flutter/material.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:provider/provider.dart';

class All_Product_Page extends StatefulWidget {
  const All_Product_Page({Key? key}) : super(key: key);

  @override
  State<All_Product_Page> createState() => _All_Product_PageState();
}

class _All_Product_PageState extends State<All_Product_Page> {
  final _key=GlobalKey<ScaffoldState>();
 Box? box = Hive.box('productBox');
  @override
  Widget build(BuildContext context) {
    final all_productlist=Provider.of<All_Product_Provider>(context).allproductlist;
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.white,
        drawer: Custom_Drawer_Page(),
        endDrawer: End_Add_to_cart_Drawer(),
        key: _key,

        // bottomNavigationBar: CustomNavigationBarPage(Home_color: Colors.grey, Produc_tColor: Colors.white, Category_color: Colors.grey, setting_Color: Colors.grey),
        body: CustomScrollView(
          slivers: [
            Custom_Appbar(
              onPressed: () {
                _key.currentState!.openDrawer();
              },
              End_Add_To_Cart_Drawer: () {
                _key.currentState!.openEndDrawer();
              },
            ),
            SliverPadding(
              padding: EdgeInsets.only(
              top: 10,
              bottom: 0,
              left: 20,
              right: 20,
            ),
              sliver: SliverGrid(
                gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
                maxCrossAxisExtent: 300,
                mainAxisSpacing: 16,
                crossAxisSpacing: MediaQuery.of(context).size.width*0.04,
                mainAxisExtent: 285,

              ),
                delegate: SliverChildBuilderDelegate((context, index){
                  return InkWell(
                    onTap: () {
                      Navigator.push(context, MaterialPageRoute(builder: (context) => One_Product_Details(
                        // qty: all_productlist[index].qty,
                        cashbackPercent: all_productlist[index].cashbackPercent,
                        productCode: all_productlist[index].productCode,
                        mainImage: all_productlist[index].mainImage,
                        mainPrice: all_productlist[index].mainPrice,
                        productName: all_productlist[index].productName,
                        salePrice: all_productlist[index].salePrice,
                        productDescription: all_productlist[index].productDescription,
                        cashbackAmount: all_productlist[index].cashbackAmount,
                        productSlNo: all_productlist[index].productSlNo,
                        qty: all_productlist[index].qty,
                      ),));
                    },
                    child: Card(
                      elevation: 8.0,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.only(
                            topRight: Radius.circular(20),
                            topLeft: Radius.circular(20)
                          )
                        ),
                        child: Custom_Card_Page(
                          qty: "${all_productlist[index].qty}",
                          mainImage:"https://bigbuy.com.bd/uploads/products/small_image/${all_productlist[index].mainImage}" ,
                          productName:"${all_productlist[index].productName}",
                          mainPrice: "${all_productlist[index].mainPrice}",
                          productDescription:"${all_productlist[index].productDescription}" ,
                          salePrice: "${all_productlist[index].salePrice}",
                          productCode: "${all_productlist[index].productCode}",
                          cashbackPercent: "${all_productlist[index].cashbackPercent}",
                          cashbackAmount: "${all_productlist[index].cashbackAmount}",

                        )),
                  );
              },childCount: all_productlist.length),),
            ),
            SliverToBoxAdapter(
              child: BigBuyFooter(),
            ),
          ],
        ),
      ),
    );
  }
}
