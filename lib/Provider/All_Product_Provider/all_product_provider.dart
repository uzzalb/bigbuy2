import 'package:bigbuy/API_Model_Class/all_barnd_model_class.dart';
import 'package:bigbuy/API_Model_Class/all_product_model_class.dart';
import 'package:bigbuy/API_Model_Class/api_product_color.dart';
import 'package:bigbuy/API_Model_Class/area_wise_delivery_charge.dart';
import 'package:bigbuy/API_Model_Class/category_name_list_model.dart';
import 'package:bigbuy/API_Model_Class/product_color_id_wise_size.dart';
import 'package:bigbuy/Api_Integration/api_integration_all_product.dart';
import 'package:bigbuy/Api_Integration/new_arrivel_product_api_integration.dart';
import 'package:bigbuy/Api_Integration/subcategory_product.dart';
import 'package:flutter/material.dart';

class All_Product_Provider with ChangeNotifier{
/////////////////////////////////////////////////////All Product Loist Providewr///////////////
  List<All_Product_Model> allproductlist = [];
  getCategories(context) async {
    allproductlist =await Api_All_Product_Integration.getOrder(context);
    notifyListeners();
  }
  ///////////////////////////////////////////////Brand list ////////////////////
  List<Brand_List_Model_Class> brandList=[];
  getBrand(context)async{
    brandList=await Api_All_Product_Integration.getBrand(context);
    notifyListeners();
  }
  ///////////////////////////////////Get All Category Name///////////////////////////

List<Category_Name_List_Model> CategoryNameList=[];
  getCategory_name_List(context)async{
    CategoryNameList=await Api_All_Product_Integration.getAllCategoryNameList(context);
    notifyListeners();
  }
  ///////////////////////////////////Get Hot Deal |product///////////////////////////

  List<All_Product_Model> all_hotdeal_product=[];
  get_All_hot_Deal_Product_List(context)async{
    all_hotdeal_product=await Api_Hot_Deal_Product_Integration.getHotDeal(context);
    notifyListeners();
  }
////////////////////////////////////////Get New Arrivel ///////////////////////////////////////
  List<All_Product_Model> all_new_arrivel_product_List=[];
  get_All_New_Arrivel_Product(context)async{
    all_new_arrivel_product_List=await Api_Hot_Deal_Product_Integration.NewArrivel(context);
    notifyListeners();
  }


////////////////////////////////////////Get Api Model Product Color ///////////////////////////////////////
  List<Api_Model_Product_Collor> Api_Model_Product_Collor_list=[];
  Api_Model_Product_Collor_demo(context,int id)async{
    Api_Model_Product_Collor_list=await Api_Hot_Deal_Product_Integration.Api_Product_collor(context, id);
    notifyListeners();
  }

////////////////////////////////////////Get Api Model Product Color ///////////////////////////////////////
  List<Product_Color_id_wise_size_Model_Class> Product_Color_id_wise_size_Model_Class_list=[];
  Api_Product_collor_wise_Size(context,int id, color_id)async{
    Product_Color_id_wise_size_Model_Class_list=await Api_Hot_Deal_Product_Integration.Api_Product_collor_Id_wise_Size(context, id,color_id);
    notifyListeners();
  }

////////////////////////////////////////Get Api Model Product Color ///////////////////////////////////////
  List<Api_area_wise_delivery_charse> Api_area_wise_delivery_charse_list=[];
  Api_area_wise_delivery_charge(context)async{
    Api_area_wise_delivery_charse_list=await Api_Hot_Deal_Product_Integration.Api_area_wise_delivery_charge(context);
    notifyListeners();
  }




}