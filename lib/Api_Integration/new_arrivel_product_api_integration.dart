import 'dart:convert';

import 'package:bigbuy/API_Model_Class/all_product_model_class.dart';
import 'package:bigbuy/API_Model_Class/api_product_color.dart';
import 'package:bigbuy/API_Model_Class/area_wise_delivery_charge.dart';
import 'package:bigbuy/API_Model_Class/product_color_id_wise_size.dart';
import 'package:bigbuy/Provider/All_Product_Provider/all_product_provider.dart';
import 'package:bigbuy/constants.dart';
import 'package:dio/dio.dart';
import 'package:provider/provider.dart';

class Api_Hot_Deal_Product_Integration{

  static Future<dynamic> NewArrivel(context)async{
    List<All_Product_Model> NewArrivel_Product=[];
    try{
      String link="${Baseurl}get_web_products";
      Response response=await Dio().post(link,data:
      {
        "is_new_arrival":true,
        "limit":9
      },
      );
      print("New Arrivellllllllllllllllllllllllllllllll");
       print(response.data);
      print("New Arrivellllllllllllllllllllllllllllllll");
      if(response.statusCode==200){
        final item=jsonDecode(response.data);
        All_Product_Model all_product_model;
        for(var i in item){
          all_product_model=All_Product_Model.fromJson(i);
          NewArrivel_Product.add(all_product_model);
        //  print(NewArrivel_Product);
        }
      }
    }catch(e){print("Hot Deal Catch Error is $e");}
    return NewArrivel_Product;
  }
  //////////////////////////////////////////////HOT DEAL pRODUCT//////////
  static Future<dynamic> getHotDeal(context)async{
    List<All_Product_Model> hotdeal_list=[];
    try{
      String link="${Baseurl}get_web_products";
      Response response=await Dio().post(link,data:{"is_hot_deals":true});
      print("HOTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT");
        print(response.data);
      print("HOTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT");
      if(response.statusCode==200){
        final item=jsonDecode(response.data);
        All_Product_Model all_product_model;
        for(var i in item){
          all_product_model=All_Product_Model.fromJson(i);
          hotdeal_list.add(all_product_model);

        }
      }
    }catch(e){print("Hot Deal Catch Error is $e");}
    return hotdeal_list;
  }




///////////////////Api_Product_collor//////////////////////

  static   Api_Product_collor(context,int id)async{
    List<Api_Model_Product_Collor> Api_Model_Product_Collor_List=[];
    try{
      String link="${Baseurl}get_web_product_color";
      Response response=await Dio().post(link,data:
      {"product_id": id}
      );
      print("color imageggggggggggggggggggggggggggg");
    print(response.data);
      print("color imageggggggggggggggggggggggggggg");
      if(response.statusCode==200){
        final item=jsonDecode(response.data);
        Provider.of<All_Product_Provider>(context,listen: false).Api_Product_collor_wise_Size(context, id,int.parse("${item[0]["color_id"]}"));
        Api_Model_Product_Collor api_model_product_collor;
        for(var i in item){
          api_model_product_collor=Api_Model_Product_Collor.fromJson(i);
          Api_Model_Product_Collor_List.add(api_model_product_collor);
        }
      }
    }catch(e){print("Product Color is  $e");}
    return Api_Model_Product_Collor_List;
  }


///////////////////Api_Product_collor_Id_wise_Size//////////////////////

  static   Api_Product_collor_Id_wise_Size(context,int id,color_id)async{
    List<Product_Color_id_wise_size_Model_Class> Product_Color_id_wise_size_Model_Class_List=[];
    try{
      String link="${Baseurl}get_web_product_color_size";
      Response response=await Dio().post(link,data:
      {
        "product_id": id,
        "color_id" :color_id
      }
      );
      print("SIZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ");
     print(response.data);
      print("SIZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZT");
      if(response.statusCode==200){
        final item=jsonDecode(response.data);
        Product_Color_id_wise_size_Model_Class product_color_id_wise_size_model_class;
        for(var i in item){
          product_color_id_wise_size_model_class=Product_Color_id_wise_size_Model_Class.fromJson(i);
          Product_Color_id_wise_size_Model_Class_List.add(product_color_id_wise_size_model_class);
       //   print(Product_Color_id_wise_size_Model_Class_List);
        }
      }
    }catch(e){print("Product Color id wise size$e");}
    return Product_Color_id_wise_size_Model_Class_List;
  }


///////////////////Api_Product_collor_Id_wise_Size//////////////////////

  static   Api_area_wise_delivery_charge(context)async{
    List<Api_area_wise_delivery_charse> Api_area_wise_delivery_charse_list=[];
    try{
      String link="${Baseurl}get_web_area";
      Response response=await Dio().get(link,

      );
      print("get_web_areaZzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz");
     // print(response.data);
      print("get_web_areazzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz");
      if(response.statusCode==200){
        final item=jsonDecode(response.data);
        Api_area_wise_delivery_charse api_area_wise_delivery_charge;
        for(var i in item){
          api_area_wise_delivery_charge=Api_area_wise_delivery_charse.fromJson(i);
          Api_area_wise_delivery_charse_list.add(api_area_wise_delivery_charge);
        //  print(Api_area_wise_delivery_charse_list);
        }
      }
    }catch(e){print("Product Color id wise size$e");}
    return Api_area_wise_delivery_charse_list;
  }

}